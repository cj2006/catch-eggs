﻿using UnityEngine;
using System.Collections;

public class LevelRef : MonoBehaviour {

	private static LevelRef instanceValue;
	public static LevelRef Instance {
		get {
			if( instanceValue == null ) {
				instanceValue = FindObjectOfType<LevelRef>();
			}
			return instanceValue;
		}
	}

	public AnvilSpawner m_anvilSpawner;
	public OnStart m_onStart;
}
