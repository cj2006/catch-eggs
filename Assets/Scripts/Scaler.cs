﻿using UnityEngine;
using System.Collections;

public class Scaler : MonoBehaviour 
{
	// Set multiplier for determine scaling
	public float sizeMultiplier;

	// Scale size of game objects
	void Start () 
	{
		float size = Camera.main.orthographicSize * Camera.main.aspect * sizeMultiplier*0.005f;
		transform.localScale = new Vector3 (size, size, size);
	}
}
