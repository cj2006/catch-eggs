﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ControllBasket : MonoBehaviour 
{
	public float speed;
	private Vector2 pos;
	private Vector2 border;

	void Start()
	{
		// Create vector for finding screen bounds
		border = new Vector2(-Camera.main.aspect * Camera.main.orthographicSize + renderer.bounds.size.x / 2, 
		                     Camera.main.aspect * Camera.main.orthographicSize - renderer.bounds.size.x / 2);
	}

	void Update () 
	{
		// Move basket left or right when mouse change position
		pos.x = Camera.main.ScreenToWorldPoint (Input.mousePosition).x;
		pos.y = transform.position.y;
		transform.position = pos;

		// Set left-right screen bounds:
		if (transform.position.x <= border.x) transform.position = new Vector2(border.x, transform.position.y);
		else if (transform.position.x >= border.y) transform.position = new Vector2(border.y, transform.position.y);
	}
}