﻿using UnityEngine;
using System.Collections;
using System.Threading;
using UnityEngine.UI;


public class RottenEgg : MonoBehaviour 
{
	// Chance of falling egg
	public float chanceOfFalling;
	// Acceleration of eggs
	public float accelerate;
	// Time in which egg is trigger
	private float timer;
	// Force added to egg
	private Vector2 force;
	// Direction left or right
	private Vector2 direction;
	private float[] directionArray = new float[2] { 1, -1 };
	// Check if egg touch floor
	private bool isOnTheFloor;
	// Create link to GameRef
	private GameObject gameRef;
	
	void Start () 
	{
		// Set game object as GameRef
		gameRef = GameObject.Find ("GameRef");
		// On start egg is not on the floor
		isOnTheFloor = false;
		int rand = Random.Range(0, directionArray.Length);
		direction = new Vector2(directionArray[rand], direction.y);
		force = direction * accelerate;
		rigidbody2D.AddRelativeForce (force);
	}

	void Update () 
	{
		// Calculate time in which egg will be trigger
		if (collider2D.isTrigger)
			timer++;
		if (timer >= 40) {
			timer = 0;
			collider2D.isTrigger = false;
		}
	}
	void OnCollisionStay2D(Collision2D coll) 
	{
		// Calculate a moment the egg will fall down if egg is on the shelf
		float rand = Random.Range(0.0F, 1.0F);
		if ((rand >= chanceOfFalling) && (coll.gameObject.tag == "Shelf"))
		{
			collider2D.isTrigger = true;
		}
	}
	void OnCollisionEnter2D(Collision2D coll) 
	{
		// Rotten egg is on the floor
		if (coll.gameObject.tag == "Floor") {
			isOnTheFloor = true;
		}
		// If rotten egg collide with basket while not on the floor, it will destroy all catched eggs and score will be 0
		if ((coll.gameObject.tag == "Basket") && (isOnTheFloor == false))
		{
			// Count of rotted egg to calculate when stop the game
			gameRef.GetComponent<OnStart>().countOfRottedEggs += gameRef.GetComponent<OnStart>().countOfCatchedEggs;
			// Count of catched eggs becomes 0
			gameRef.GetComponent<OnStart>().countOfCatchedEggs = 0;
			GameObject.Find("guiScore").GetComponent<Text>().text = "Score: " + gameRef.GetComponent<OnStart>().countOfCatchedEggs;

			Destroy(gameObject);
		}
	}
	// When rotten egg collide with trigger floor it will be destroyed
	void OnTriggerEnter2D(Collider2D coll) 
	{
		if (coll.gameObject.tag == "Floor") {
			Destroy(gameObject);
		}
	}
}