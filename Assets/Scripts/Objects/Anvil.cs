﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Anvil : MonoBehaviour 
{
	// Speed with which anvil falling
	public float anvilSpeed;
	// Prefab of result panel, which spawn if anvil collide with basket
	public GameObject resultPanelPrefab;
	public GameObject resultPanelPrefabEndless;
	// Variable which set X position of anvil
	private float anvilSpawnX;
	// Variable for finding borders of scene
	private Vector2 border;
	// Check if it is endless mode
	private bool isEndless;
	// Create link to GameRef
	//private GameObject gameRef;

	void Start () 
	{
		// Set game object as GameRef
		//gameRef = GameObject.Find ("GameRef");
		// Set this variable value 
		isEndless = LevelRef.Instance.m_anvilSpawner.isEndless;
		// Finding where on X we will put anvil
		border = new Vector2(-Camera.main.aspect * Camera.main.orthographicSize + renderer.bounds.size.x / 2, 
		                     Camera.main.aspect * Camera.main.orthographicSize - renderer.bounds.size.x / 2);
		anvilSpawnX = Random.Range(border.x, border.y);
		// Set anvil to start position
		transform.position = new Vector2 (anvilSpawnX, Camera.main.orthographicSize * 2);
		// Give speed to anvil
		// If endless mode
		if (isEndless) {
			float acceleration = 0.2f * LevelRef.Instance.m_onStart.countAnvils;
			if (acceleration < 40) {
				rigidbody2D.velocity = new Vector2 (0, -(anvilSpeed + acceleration));
			}
		}
		// If normal mode
		else {
			rigidbody2D.velocity = new Vector2 (0, -anvilSpeed);
		}
		Destroy (gameObject, 5);
	}
	// Function that destroy all ingame objects with tag "tag"
	private void Destroyer(string tag)
	{
		GameObject[] ArrayOfObjects = GameObject.FindGameObjectsWithTag(tag);
		for(int i=0;i< ArrayOfObjects.Length;i++)
		{
			Destroy(ArrayOfObjects[i].gameObject);
		}
	}
	// Function that disable rotten egg spawner scripts on chickens
	private void RottenEggSpawnerDisabler()
	{
		GameObject[] ArrayOfObjects = GameObject.FindGameObjectsWithTag("Chicken");
		for(int i=0;i< ArrayOfObjects.Length;i++)
		{
			ArrayOfObjects[i].gameObject.GetComponent<RottenEggSpawner>().enabled = false;
		}
	}
	// Function that disable egg spawner scripts on chickens
	private void EggSpawnerDisabler()
	{
		GameObject[] ArrayOfObjects = GameObject.FindGameObjectsWithTag("Chicken");
		for(int i=0;i< ArrayOfObjects.Length;i++)
		{
			ArrayOfObjects[i].gameObject.GetComponent<EggSpawner>().enabled = false;
		}
	}
	void OnCollisionEnter2D(Collision2D coll) 
	{
		// If anvil collide with basket
		if (coll.gameObject.tag == "Basket") 
		{
			// If it is normal game
			if(!isEndless)
			{
				// Activate vibration
				// Add vibration code here
				// Change sprite on smashed basket with anvil
				// Add code for change sprites
				// Disable basket controlls
				// Count of catched eggs becomes 0
				LevelRef.Instance.m_onStart.countOfCatchedEggs = 0;
				// Disable score
				//Destroy (GameObject.Find ("Score"));
				GameObject.Find("guiScore").GetComponent<Text>().text = "Score: 0";
				// Disable basket controlls
				GameObject.Find("Basket").GetComponent<ControllBasket>().enabled = false;
				// Disable script that spawn anvils
				LevelRef.Instance.m_anvilSpawner.enabled = false;
				// Disable scripts that spawns eggs
				EggSpawnerDisabler();
				RottenEggSpawnerDisabler();
				// Spawn result panel
				GameObject prefOptions = Instantiate(resultPanelPrefab) as GameObject;
				prefOptions.transform.SetParent(GameObject.Find("InGameCanvas").transform, false);
				// Text that will be in the result window
				GameObject.Find("ResultText").GetComponent<Text>().text = "Try Again...";
				// Destroy eggs and anvils if there are some
				Destroyer("Egg");
				Destroyer("Anvil");
			}
			// If it is endless mode
			else
			{
				// Activate vibration
				// Add vibration code here
				// Change sprite on smashed basket with anvil
				// Add code for change sprites
				// Disable basket controlls
				GameObject.Find("Basket").GetComponent<ControllBasket>().enabled = false;
				// Disable script that spawn anvils
				LevelRef.Instance.m_anvilSpawner.enabled = false;
				// Disable scripts that spawns eggs
				EggSpawnerDisabler();
				// Spawn result panel
				GameObject prefOptions = Instantiate(resultPanelPrefabEndless) as GameObject;
				prefOptions.transform.SetParent(GameObject.Find("InGameCanvas").transform, false);
				// Text that will be in the result window
				GameObject.Find("ResultText").GetComponent<Text>().text = "WOW!";
				GameObject.Find("ScoreText").GetComponent<Text>().text = "" + (LevelRef.Instance.m_onStart.countOfCatchedEggs - LevelRef.Instance.m_onStart.countOfBrokenEggs);
				// If > highscore - set as highscore
				// Add code for set highscore
				// Destroy eggs and anvils if there are some
				Destroyer("Egg");
				Destroyer("Anvil");
			}
		}
	}
}