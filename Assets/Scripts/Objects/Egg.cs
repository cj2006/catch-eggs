﻿using UnityEngine;
using System.Collections;
using System.Threading;
using UnityEngine.UI;

public class Egg : MonoBehaviour 
{
	// Prefab of result panel
	public GameObject resultPanelPrefab;
	// Acceleration of eggs
	public float accelerate;
	// Chance of falling egg
	public float chanceOfFalling;
	// Time in which egg is trigger
	private float timer;
	// Force added to egg
	private Vector2 force;
	// Direction left or right
	private Vector2 direction;
	private float[] directionArray = new float[2] { 1, -1 };
	// Sprite of broken egg
	public Sprite brokenEggSprite;
	// Set timer for destroying
	public int destroyTimer;
	// Create link to GameRef
	private GameObject gameRef;

	// Function that destroy all ingame objects with tag "tag"
	private void Destroyer(string tag)
	{
		GameObject[] ArrayOfObjects = GameObject.FindGameObjectsWithTag(tag);
		for(int i=0;i< ArrayOfObjects.Length;i++)
		{
			Destroy(ArrayOfObjects[i].gameObject);
		}
	}
	// Function that disable rotten egg spawner scripts on chickens
	private void RottenEggSpawnerDisabler()
	{
		GameObject[] ArrayOfObjects = GameObject.FindGameObjectsWithTag("Chicken");
		for(int i=0;i< ArrayOfObjects.Length;i++)
		{
			ArrayOfObjects[i].gameObject.GetComponent<RottenEggSpawner>().enabled = false;
		}
	}
	// Function that call on level ending
	private void OnLevelEnd()
	{
		// Disable basket controlls
		GameObject.Find("Basket").GetComponent<ControllBasket>().enabled = false;
		// Disable script that spawn anvils
		gameRef.GetComponent<AnvilSpawner>().enabled = false;
		// Disable scripts that spawns eggs
		RottenEggSpawnerDisabler ();
		// Disable score
		//Destroy (GameObject.Find ("Score"));
		// Spawn result panel
		GameObject prefOptions = Instantiate(resultPanelPrefab) as GameObject;
		prefOptions.transform.SetParent(GameObject.Find("InGameCanvas").transform, false);
		// Choose what text will be in the result window
		int amount = gameRef.GetComponent<OnStart>().amountOfEggs;
		int count = gameRef.GetComponent<OnStart>().countOfCatchedEggs;
		// If catch all eggs - get Gold
		if (count == amount) {
			GameObject.Find ("ResultText").GetComponent<Text>().text = "Excellent!!!";
			GameObject.Find ("BronzeEgg").GetComponent<Image>().enabled = true;
			GameObject.Find ("SilverEgg").GetComponent<Image>().enabled = true;
			GameObject.Find ("GoldEgg").GetComponent<Image>().enabled = true;
			string level = "level" + gameRef.GetComponent<OnStart>().levelNumber;
			if (PlayerPrefs.GetInt(level) < 3)
				PlayerPrefs.SetInt(level, 3);
		} 
		// If catch (all eggs-1) - get Silver
		else if (count == amount - 1) {
			GameObject.Find ("ResultText").GetComponent<Text>().text = "Perfect!!";
			GameObject.Find ("BronzeEgg").GetComponent<Image>().enabled = true;
			GameObject.Find ("SilverEgg").GetComponent<Image>().enabled = true;
			string level = "level" + gameRef.GetComponent<OnStart>().levelNumber;
			if (PlayerPrefs.GetInt(level) < 2)
				PlayerPrefs.SetInt(level, 2);
		} 
		// If catch (all eggs-2) - get Bronze
		else if (count == amount - 2) {
			GameObject.Find ("ResultText").GetComponent<Text>().text = "Good!!";
			GameObject.Find ("BronzeEgg").GetComponent<Image>().enabled = true;
			string level = "level" + gameRef.GetComponent<OnStart>().levelNumber;
			if (PlayerPrefs.GetInt(level) < 1)
				PlayerPrefs.SetInt(level, 1);
		}
		// You dont clear level if catch less than (all eggs-2)
		else {
			GameObject.Find("ResultText").GetComponent<Text>().text = "Try Again...";
		}
		// Destroy eggs and anvils if there are some
		Destroyer("Egg");
		Destroyer("Anvil");
	}
	void Start () 
	{
		// Set game object as GameRef
		gameRef = GameObject.Find ("GameRef");
		// Choose direction of egg (left or right)
        int rand = Random.Range(0, directionArray.Length);
        direction = new Vector2(directionArray[rand], direction.y);
		// Set force that would be applied to egg
		force = direction * accelerate;
		// Apply force to egg
		rigidbody2D.AddRelativeForce (force);
	}
	void Update () 
	{
		// Calculate time in which egg will be trigger
		if (collider2D.isTrigger)
			timer++;
		if (timer >= 40) {
			timer = 0;
			collider2D.isTrigger = false;
		}
	}
	void OnCollisionStay2D(Collision2D coll) 
	{
		// Calculate a moment the egg will fall down if egg is on the shelf, 
		float rand = Random.Range(0.0F, 1.0F);
		if ((rand >= chanceOfFalling) && (coll.gameObject.tag == "Shelf"))
		{
			collider2D.isTrigger = true;
		}
	}
	void OnCollisionEnter2D(Collision2D coll) 
	{
		// If egg fall down on the floor it will smash
		if (coll.gameObject.tag == "Floor") {
			// Activate vibration
			// Add vibration code here
			// Increase count of broken eggs
			gameRef.GetComponent<OnStart>().countOfBrokenEggs++;
			// Make it kinematic so it wont move
			rigidbody2D.isKinematic = true;
			// Zero rotation
			transform.rotation = Quaternion.identity;
			// Change sprite to broken egg
			GetComponent<SpriteRenderer> ().sprite = this.brokenEggSprite;
			// Set it's size
			float sizeMultiplier = gameObject.GetComponent<Scaler>().sizeMultiplier * 1.5f;
			float size = Camera.main.orthographicSize * Camera.main.aspect * sizeMultiplier*0.005f;
			transform.localScale = new Vector3 (size, size, size);
			// Destroy broken egg after some time
			Destroy (gameObject, destroyTimer);
			// If all eggs out of game result panel will be spawned
			if(gameRef.GetComponent<OnStart>().countOfBrokenEggs + gameRef.GetComponent<OnStart>().countOfRottedEggs + gameRef.GetComponent<OnStart>().countOfCatchedEggs >= gameRef.GetComponent<OnStart>().amountOfEggs)
			{
				OnLevelEnd();
			}
		}
       	// If egg collide with basket, it will be destroyed and score will be increased by 1 
		if (coll.gameObject.tag == "Basket") 
		{
			gameRef.GetComponent<OnStart>().countOfCatchedEggs++;
			GameObject.Find("guiScore").GetComponent<Text>().text = "Score: " + gameRef.GetComponent<OnStart>().countOfCatchedEggs;
			// If all eggs out of game result panel will be spawned
			if(gameRef.GetComponent<OnStart>().countOfBrokenEggs + gameRef.GetComponent<OnStart>().countOfRottedEggs + gameRef.GetComponent<OnStart>().countOfCatchedEggs >= gameRef.GetComponent<OnStart>().amountOfEggs)
			{
				OnLevelEnd();
			}
			Destroy(gameObject);
		}
	}
}
