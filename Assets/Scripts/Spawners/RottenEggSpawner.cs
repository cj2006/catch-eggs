﻿using UnityEngine;
using System.Collections;

public class RottenEggSpawner : MonoBehaviour 
{
	// Prefab of rotten egg
	public GameObject rottenEggPrefab;
	// Create link to GameRef
	private GameObject gameRef;
	// Timer
	private float rottenEggTimer;
	// Random time for rotten egg respawn
	private int rottenEggSpawn;

	// On start create a timers for rotten egg spawn
	void Start () 
	{
		// Set game object as GameRef
		gameRef = GameObject.Find ("GameRef");
		// Set random frame rate after which new rotten egg will be spawned
		rottenEggSpawn = Random.Range(300, 500);
	}
	
	// Update is called once per frame
	void Update () {
		rottenEggTimer++;
		// Wait rottenEggSpawn amount of time and spawn new rotten egg
		if ((rottenEggTimer >= rottenEggSpawn) && (gameRef.GetComponent<OnStart>().countOfRottenEggs < gameRef.GetComponent<OnStart>().amountOfRottenEggs))
		{
			gameRef.GetComponent<OnStart>().countOfRottenEggs++;
			// Spawn new rotten egg
			Instantiate(rottenEggPrefab, transform.position, Quaternion.identity);
			// Reset timer
			rottenEggTimer = 0;
			// Set random frame rate after which new rotten egg will be spawned
			rottenEggSpawn = Random.Range(300, 500);
		}
	}
}
