﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EggSpawner : MonoBehaviour 
{
	// Prefab for egg
	public GameObject eggPrefab;
	// Create link to GameRef
	private GameObject gameRef;
	// Random time for egg respawn
	private int eggSpawn;
	// Timer
	private float timer;

	// On start create a timers for egg spawn
	void Start () 
	{
		// Set game object as GameRef
		gameRef = GameObject.Find ("GameRef");
		// Set random frame rate after which new egg will be spawned
		eggSpawn = Random.Range(100, 200);
	}
	// Wait timer amount of time and spawn new egg
	void Update () 
	{
		timer++;
		// Wait eggSpawn amount of time and spawn new egg
		if (timer >= eggSpawn)
		{
			if (gameRef.GetComponent<OnStart>().countEggs < gameRef.GetComponent<OnStart>().amountOfEggs)
	        {
				gameRef.GetComponent<OnStart>().countEggs++;
				// Spawn new egg
				Instantiate(eggPrefab,transform.position, Quaternion.identity);
				// Set random frame rate after which new egg will be spawned
				eggSpawn = Random.Range(100, 200);
				// Reset timer
	            timer = 0;
			}
		}
	}
}
