﻿using UnityEngine;
using System.Collections;

public class AnvilSpawner : MonoBehaviour 
{
	// Anvil prefab
	public GameObject anvilPrefab;
	// Create link to GameRef
	private GameObject gameRef;
	// Set time in which anvil will spawn
	public int anvilSpawn;
	// Timer variable
	private float timer;
	// Check if it is endless mode
	public bool isEndless = false;

	void Start()
	{
		// Set game object as GameRef
		gameRef = GameObject.Find ("GameRef");
	}
	// Update is called once per frame
	void Update () 
	{
		timer++;
		// Wait eggSpawn amount of time and spawn new egg
		if (timer >= anvilSpawn) {
			if (gameRef.GetComponent<OnStart> ().countAnvils < gameRef.GetComponent<OnStart> ().amountOfAnvils) {
				gameRef.GetComponent<OnStart> ().countAnvils++;
				// Spawn new anvil
				Instantiate (anvilPrefab, transform.position, Quaternion.identity);
				// Reset timer for new anvils
				timer = 0;
				// If it is endless mode time between new anvils will decrease with each anvil spawned till limit
				if((isEndless)&&(anvilSpawn>200))
				{
					anvilSpawn -= 10;
				}
			}
		}
	}
}
