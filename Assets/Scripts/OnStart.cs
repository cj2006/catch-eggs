﻿using UnityEngine;
using System.Collections;

public class OnStart : MonoBehaviour 
{
	// Amount of eggs that should be spawned
	public int amountOfEggs;
	// Amount of rotten eggs that should be spawned
	public int amountOfRottenEggs;
	// Amount of anvils that should be spawned
	public int amountOfAnvils;
	// Count of already spawned eggs
	public int countEggs {get;set;}
	// Count of already spawned anvils
	public int countAnvils {get;set;}
	// Count of eggs being caught
	public int countOfCatchedEggs {get;set;}
	// Count of eggs being rotted
	public int countOfRottedEggs {get;set;}
	// Count of already spawned rotten eggs
	public int countOfRottenEggs {get;set;}
	// Count of eggs being broken
	public int countOfBrokenEggs {get;set;}
	// Sets number of level
	public int levelNumber;


	void Start () 
	{
		// Eggs don't collide with each other
		Physics2D.IgnoreLayerCollision(8, 8);
		Physics2D.IgnoreLayerCollision(10, 8);
		Physics2D.IgnoreLayerCollision(10, 9);
		Time.timeScale = 1;
	}
}
