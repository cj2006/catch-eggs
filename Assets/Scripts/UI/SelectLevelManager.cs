﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SelectLevelManager : MonoBehaviour 
{
	// Prefab of main menu
	public GameObject mainMenuPrefab;
	// Number of this level
	public int levelNumber;

	// Function for enabling new levels
	void Start () {
		string level = "level"+(levelNumber-1);
		if (PlayerPrefs.GetInt (level) > 0) {
			//GetComponent<Image> ().sprite = this.sprite;
			// Disable locked image
			string findObject = "Level"+levelNumber+"/Button/Image";
			GameObject.Find(findObject).GetComponent<Image>().enabled = false;
			// Make button clickable
			GetComponent<Button> ().interactable = true;
			if(PlayerPrefs.GetInt (level) == 1)
			{
				// Enable bronze egg
				findObject = "Level"+(levelNumber-1)+"/Bronze/BronzeEgg";
				GameObject.Find(findObject).GetComponent<Image>().enabled = true;
			}
			if(PlayerPrefs.GetInt (level) == 2)
			{
				// Enable bronze and silver eggs
				findObject = "Level"+(levelNumber-1)+"/Bronze/BronzeEgg";
				GameObject.Find(findObject).GetComponent<Image>().enabled = true;
				findObject = "Level"+(levelNumber-1)+"/Silver/SilverEgg";
				GameObject.Find(findObject).GetComponent<Image>().enabled = true;
			}
			if(PlayerPrefs.GetInt (level) == 3)
			{
				// Enable bronze, silver and gold eggs
				findObject = "Level"+(levelNumber-1)+"/Bronze/BronzeEgg";
				GameObject.Find(findObject).GetComponent<Image>().enabled = true;
				findObject = "Level"+(levelNumber-1)+"/Silver/SilverEgg";
				GameObject.Find(findObject).GetComponent<Image>().enabled = true;
				findObject = "Level"+(levelNumber-1)+"/Gold/GoldEgg";
				GameObject.Find(findObject).GetComponent<Image>().enabled = true;
			}
		}
	}

	// Function that starts chosen level
	public void StartLevel()
	{
		Application.LoadLevel (levelNumber);
	}
	// Function that close level selecting panel
	public void CloseSelectLevelPanel()
	{
		// Instantiate main menu:
		GameObject prefOptions = Instantiate(mainMenuPrefab) as GameObject;
		prefOptions.transform.SetParent(GameObject.Find("Canvas").transform, false);
		Destroy (GameObject.Find("SelectLevelPanel(Clone)"));
	}
}
