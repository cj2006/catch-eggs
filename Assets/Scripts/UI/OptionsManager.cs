﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class OptionsManager : MonoBehaviour 
{
	// Prefab of main menu
	public GameObject mainMenuPrefab;

	void Start()
	{
		// If sound is disabled, you could press On for activate it
		if (PlayerPrefs.GetInt ("SoundActive") == 0) {
			GameObject.Find ("SoundOn").GetComponent<Button> ().interactable = true;
			GameObject.Find ("SoundOff").GetComponent<Button> ().interactable = false;
		} 
		// else you could press Off for disable it
		else 
		{
			GameObject.Find ("SoundOn").GetComponent<Button> ().interactable = false;
			GameObject.Find ("SoundOff").GetComponent<Button> ().interactable = true;
		}
		// If vibration is disabled, you could press On for activate it
		if (PlayerPrefs.GetInt ("VibrationActive") == 0) {
			GameObject.Find ("VibrationOn").GetComponent<Button> ().interactable = true;
			GameObject.Find ("VibrationOff").GetComponent<Button> ().interactable = false;
		} 
		// else you could press Off for disable it
		else 
		{
			GameObject.Find ("VibrationOn").GetComponent<Button> ().interactable = false;
			GameObject.Find ("VibrationOff").GetComponent<Button> ().interactable = true;
		}
		// If graphics set to low, you could press High for change it to high
		if (PlayerPrefs.GetInt ("GraphicsQuality") == 0) {
			GameObject.Find ("GraphicsHigh").GetComponent<Button> ().interactable = true;
			GameObject.Find ("GraphicsLow").GetComponent<Button> ().interactable = false;
		} 
		// else you could press Low for change it on low
		else 
		{
			GameObject.Find ("GraphicsHigh").GetComponent<Button> ().interactable = false;
			GameObject.Find ("GraphicsLow").GetComponent<Button> ().interactable = true;
		}
	}
	// Function that close options and open main menu
	public void CloseOption()
	{
		// Instantiate main menu:
		GameObject prefOptions = Instantiate(mainMenuPrefab) as GameObject;
		prefOptions.transform.SetParent(GameObject.Find("Canvas").transform, false);
		Destroy (GameObject.Find("Options(Clone)"));
	}
	// Clear all progress
	public void ClearProgressButton()
	{
		PlayerPrefs.DeleteAll ();
	}
	// Function that turn sound on
	public void SoundOnButton()
	{
		// If at the moment there is no sound, turn it on
		// Code for turning sound on
		PlayerPrefs.SetInt ("SoundActive", 1);
		// Disable this button
		gameObject.GetComponent<Button> ().interactable = false;
		// and enable sound off button
		GameObject.Find("SoundOff").GetComponent<Button>().interactable = true;
	}
	// function that turn sound off
	public void SoundOffButton()
	{
		// If at the moment there is sound, turn it off
		// Code for turning sound off
		PlayerPrefs.SetInt ("SoundActive", 0);
		// Disable this button
		gameObject.GetComponent<Button> ().interactable = false;
		// and enable sound on button
		GameObject.Find("SoundOn").GetComponent<Button>().interactable = true;
	}
	// Function that turn vibration on
	public void VibrationOnButton()
	{
		// If at the moment there is no vibration, turn it on
		// Code for turning vibration on
		PlayerPrefs.SetInt ("VibrationActive", 1);
		// Disable this button
		gameObject.GetComponent<Button> ().interactable = false;
		// and enable sound off button
		GameObject.Find("VibrationOff").GetComponent<Button>().interactable = true;
	}
	// Function that turn vibration off
	public void VibrationOffButton()
	{
		// If at the moment there is no vibration, turn it off
		// Code for turning vibration off
		PlayerPrefs.SetInt ("VibrationActive", 0);
		// Disable this button
		gameObject.GetComponent<Button> ().interactable = false;
		// and enable sound off button
		GameObject.Find("VibrationOn").GetComponent<Button>().interactable = true;
	}
	// Function that sets graphics to low
	public void GraphicsLowButton()
	{
		// If at the moment there is high graphics, set it to low
		// Code for setting graphics to low
		PlayerPrefs.SetInt ("GraphicsQuality", 0);
		// Disable this button
		gameObject.GetComponent<Button> ().interactable = false;
		// and enable sound off button
		GameObject.Find("GraphicsHigh").GetComponent<Button>().interactable = true;
	}
	// Function that sets graphics to high
	public void GraphicsHighButton()
	{
		// If at the moment there is low graphics, set it to high
		// Code for setting graphics to high
		PlayerPrefs.SetInt ("GraphicsQuality", 1);
		// Disable this button
		gameObject.GetComponent<Button> ().interactable = false;
		// and enable sound off button
		GameObject.Find("GraphicsLow").GetComponent<Button>().interactable = true;
	}
}
