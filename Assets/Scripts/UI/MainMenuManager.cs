﻿using UnityEngine;
using System.Collections;

public class MainMenuManager : MonoBehaviour 
{
	// Prefab of option menu
	public GameObject optionsPrefab;
	// Prefab of confirming exit window
	public GameObject confirmExitPanelPrefab;
	// Prefab of selecting level panel
	public GameObject selectLevelPanelPrefab;

	// Function that start endless game mode
	public void StartEndlessGame()
	{
		Application.LoadLevel (6);
	}	
	// Function that close game
	public void ExitGame()
	{
		Application.Quit();
	}
	// Function that call on exiting game to confirm or not exit
	public void OpenConfirmPanel()
	{
		GameObject prefOptions = Instantiate(confirmExitPanelPrefab) as GameObject;
		prefOptions.transform.SetParent(GameObject.Find("Canvas").transform, false);
	}
	// Dont leave the game
	public void CloseConfirmPanel()
	{
		Destroy (GameObject.Find("ConfirmExitPanel"));
		Destroy (GameObject.Find("ConfirmExitPanel(Clone)"));
	}
	// Function that opens Option menu
	public void OpenOptions()
	{
		// Instantiate options menu:
		GameObject prefOptions = Instantiate(optionsPrefab) as GameObject;
		prefOptions.transform.SetParent(GameObject.Find("Canvas").transform, false);
		Destroy (GameObject.Find("MainMenu"));
		Destroy (GameObject.Find("MainMenu(Clone)"));
	}
	// Function that open level selecting panel
	public void OpenSelectLevelPanel()
	{
		// Instantiate select level panel:
		GameObject prefOptions = Instantiate(selectLevelPanelPrefab) as GameObject;
		prefOptions.transform.SetParent(GameObject.Find("Canvas").transform, false);
		Destroy (GameObject.Find("MainMenu"));
		Destroy (GameObject.Find("MainMenu(Clone)"));
	}
}
