﻿using UnityEngine;
using System.Collections;

public class PauseMenuManager : MonoBehaviour 
{
	// Prefab for pause panel
	public GameObject pausePanelPrefab;
	// Prefab for pause button
	public GameObject pauseButtonPrefab;
	// Prefab for confirm panel
	public GameObject confirmPanelPrefab;

	// Function that pause scripts on chickens
	private void ChickenScriptsPauser(bool param)
	{
		GameObject[] ArrayOfObjects = GameObject.FindGameObjectsWithTag("Chicken");
		for(int i=0;i< ArrayOfObjects.Length;i++)
		{
			ArrayOfObjects[i].GetComponent<EggSpawner>().enabled = param;
			ArrayOfObjects[i].GetComponent<RottenEggSpawner>().enabled = param;
		}
	}
	// Function that open pause panel
	public void OpenPausePanel()
	{
		// Instantiate pause panel:
		GameObject prefOptions = Instantiate(pausePanelPrefab) as GameObject;
		prefOptions.transform.SetParent(GameObject.Find("InGameCanvas").transform, false);
		// Destroy pause button
		Destroy (GameObject.Find("PauseButton"));
		Destroy (GameObject.Find("PauseButton(Clone)"));
		// Set time scale to 0, so the game is paused
		Time.timeScale = 0;
		// Disable scripts
		GameObject.Find("Basket").GetComponent<ControllBasket>().enabled = false;
		GameObject.Find("GameRef").GetComponent<AnvilSpawner>().enabled = false;
		ChickenScriptsPauser (false);
	}
	// Function that close 
	public void ClosePausePanel()
	{
		// Instantiate result panel:
		GameObject prefOptions = Instantiate(pauseButtonPrefab) as GameObject;
		prefOptions.transform.SetParent(GameObject.Find("InGameCanvas").transform, false);
		// Destroy pause panel
		Destroy (GameObject.Find("PausePanel"));
		Destroy (GameObject.Find("PausePanel(Clone)"));
		// Set time scale to 1, so the game is unpaused
		Time.timeScale = 1;
		// Enable scripts
		GameObject.Find("Basket").GetComponent<ControllBasket>().enabled = true;
		GameObject.Find("GameRef").GetComponent<AnvilSpawner>().enabled = true;
		ChickenScriptsPauser (true);
	}
	// Function that open confirm panel
	public void OpenConfirmPanel()
	{
		GameObject prefOptions = Instantiate(confirmPanelPrefab) as GameObject;
		prefOptions.transform.SetParent(GameObject.Find("InGameCanvas").transform, false);
	}
	// Function that close confirm panel
	public void CloseConfirmPanel()
	{
		// Destroy confirm panel
		Destroy (GameObject.Find("ConfirmPanel"));
		Destroy (GameObject.Find("ConfirmPanel(Clone)"));
	}
	// Function that open main menu
	public void OpenMainMenu()
	{
		Time.timeScale = 1;
		Application.LoadLevel (0);
	}
}
