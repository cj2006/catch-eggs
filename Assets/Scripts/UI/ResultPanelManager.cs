﻿using UnityEngine;
using System.Collections;

public class ResultPanelManager : MonoBehaviour 
{
	// Number of next level
	private int nextLevelNumber;

	void Start () 
	{
		// Set number of next level
		nextLevelNumber = GameObject.Find ("GameRef").GetComponent<OnStart> ().levelNumber + 1;
	}
	// Function that load next level
	public void NextLevel()
	{
		Application.LoadLevel (nextLevelNumber);
	}
	// Function that restart this level
	public void RestartLevel()
	{
		Time.timeScale = 1;
		Application.LoadLevel (nextLevelNumber-1);
	}
	// Function that open main menu
	public void OpenMainMenu()
	{
		Application.LoadLevel (0);
	}
}
