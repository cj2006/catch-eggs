﻿using UnityEngine;
using System.Collections;

public class Positioning : MonoBehaviour 
{
	// Set multiplier for determine the position
	public float multiplier;

	// Help with positioning of objects on screen depend on different resolution 
	void Start () 
	{
		transform.position = new Vector3 (transform.position.x, Camera.main.orthographicSize*Camera.main.aspect*multiplier/10);
	}
}
